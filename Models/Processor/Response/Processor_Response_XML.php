<?php 
namespace App\Modules\Processor\Models\Processor\Response;

/**
 * Class to parse response from xml document, get expected values after parsing
 */
class Processor_Response_XML
{
    protected $_document = null;

    /**
     * Convert HTML entities into strings
     * 
     * @param sting $value
     * @return string
     */
    public static function encode($value)
    {
        $value = mb_convert_encoding($value, 'UTF-8', mb_detect_encoding($value));
        return htmlspecialchars($value, ENT_NOQUOTES, 'UTF-8');
    }
    
    /**
     * Load the XML and parse the document
     * 
     * @param string $xml
     * @return string
     */
    public function __construct($xml)
    {
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($xml);
        
        $this->_document = Processor_Response_XML::_parse($dom);
    }
    
    /**
     * Return document
     */
    public function document()
    {
        return $this->_document;
    }
    
    /**
     * Parse the XML and return the response
     */
    protected static function _parse($curr_node)
    {
        $val_array = array();
        $type_array = array();
    
        foreach($curr_node->childNodes as $node)
        {
            if ($node->nodeType != XML_ELEMENT_NODE)
            {
                continue;
            }
            
            $val = self::_parse($node);
            
            if (array_key_exists($node->tagName, $val_array))
            {
                if (!is_array($val_array[$node->tagName]) || $type_array[$node->tagName] == 'hash')
                {
                    $existing_val = $val_array[$node->tagName];
                    unset($val_array[$node->tagName]);
                    $val_array[$node->tagName][0] = $existing_val;
                    $type_array[$node->tagName] = 'array';
                }
                $val_array[$node->tagName][] = $val;
                continue;
            }

            $val_array[$node->tagName] = $val;
            
            if (is_array($val))
            {
                $type_array[$node->tagName] = 'hash';
            }
        }
    
        return 0 == count($val_array) ? $curr_node->nodeValue: $val_array;
    }    
}